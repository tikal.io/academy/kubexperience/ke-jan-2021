# Persistence
Before you begin see how lacking of persistence looks like.
Make sure the application from previous session is running
### 0.1 Port-forward the msa-api service 

(you can look on the README.md in 200-session folder to remember how)

check the ping count
### 0.2 Restart redis pod:
get its name by running
```sh
kubectl --namespace msa-demo get pods |grep redis
```
and paste it in the delete command:
```sh
kubectl --namespace msa-demo delete pod <redis-pod-name-here>
```
Check the ping count again
### 0.3 Delete redis deployment:
```sh
kubectl --namespace msa-demo delete deployment redis
```
### 1.0 Create a persistence volume
Find out what Storage-class available in your cluster
```sh
kubectl get storageclass
```
Should yield:
```
NAME                 PROVISIONER                RECLAIMPOLICY   VOLUMEBINDINGMODE   ALLOWVOLUMEEXPANSION   AGE
standard (default)   k8s.io/minikube-hostpath   Delete          Immediate           false                  26m
```
1.0.1 Now create a file redis-data-pv.yaml with this code in it:
```yaml
kind: PersistentVolume
apiVersion: v1
metadata:
  name: redis-data
  labels:
    type: local
spec:
  capacity:
    storage: 1Gi
  accessModes:
    - ReadWriteOnce
  hostPath: 
    path: "/mnt/data"
```
1.0.2 Save the file and deploy it:
```sh
kubectl apply -f redis-data-pv.yaml
```
1.0.3 Verify a PV was created
```sh
kubectl get pv
```
Should yield:
```
NAME         CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS      CLAIM   STORAGECLASS   REASON   AGE
redis-data   1Gi        RWO            Retain           Available           standard                48m
```
### 1.1 Create `StatefulSet` and `PersistenceVolumeClaim`
Create a file redis-sts-pvc.yaml with this code in it:
```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  labels:
    run: redis
  name: redis-data
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 100Mi
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  labels:
    app: redis
  name: redis
spec:
  replicas: 1
  serviceName: redis
  selector:
    matchLabels:
      app: redis
  template:
    metadata:
      labels:
        app: redis
    spec:
      containers:
      - image: redis:5
        name: redis
        ports:
        - containerPort: 6379
        resources: {}
        volumeMounts:
        - mountPath: /data
          name: redis-data
      volumes:
      - name: redis-data
        persistentVolumeClaim:
          claimName: redis-data
```
1.1.1 Deploy the new objects
```sh
kubectl --namespace msa-demo apply -f redis-sts-pvc.yaml
```
1.1.2 Check the pv again:
```sh
kubectl get pv
```
Should yield:
```
NAME         CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM                 STORAGECLASS   REASON   AGE
redis-data   1Gi        RWO            Retain           Bound    msa-demo/redis-data   standard                51s
```
Notice the `STATUS` is now `Bound` to the claim we created in the namespace.
You can see the `PVC` using it:
```sh
kubectl --namespace msa-demo describe pvc redis-data
```
Should yield:
```
Name:          redis-data
Namespace:     msa-demo
StorageClass:  standard
Status:        Bound
Volume:        redis-data
Labels:        run=redis
Annotations:   pv.kubernetes.io/bind-completed: yes
               pv.kubernetes.io/bound-by-controller: yes
Finalizers:    [kubernetes.io/pvc-protection]
Capacity:      1Gi
Access Modes:  RWO
VolumeMode:    Filesystem
Used By:       redis-0
```
Check the persistence by looking at the ping count before and after a restart of the redis pod (see section 0.1 and 0.2).
 
### 1.2 Dynamically provisioned volumes
Delete the  `PV` `PVC` and the `StatefulSet`
```sh
kubectl --namespace msa-demo delete sts redis
kubectl --namespace msa-demo delete pvc redis-data
kubectl delete pv redis-data
```
1.2.1 make a copy of redis-sts-pvc.yaml file and name it redis-sts-pvc-t.yaml
Open the new copy and:

1.2.1.1 Delete the `persistenceVolumeClaim` definition

1.2.1.2 Remove the `Volumes` section at the end of the `Pod template` spec

1.2.1.3 Change the `replicas` to 3

1.2.1.4 Add, at the end of the file, a `VolumeClaimTemplate` definition that looks like this:
```yaml
  volumeClaimTemplates:
  - metadata:
      name: redis-data
    spec:
      accessModes:
        - ReadWriteOnce
      storageClassName: "standard"
      resources:
        requests:
          storage: 100Mi
```
The `volumeClaimTemplate` indentation must be aligned with `template`
Your file should look like this:
```yaml
apiVersion: apps/v1
kind: StatefulSet
metadata:
  labels:
    app: redis
  name: redis
spec:
  replicas: 3
  serviceName: redis
  selector:
    matchLabels:
      app: redis
  template:
    metadata:
      labels:
        app: redis
    spec:
      containers:
      - image: redis:5
        name: redis
        ports:
        - containerPort: 6379
        resources: {}
        volumeMounts:
        - mountPath: /data
          name: redis-data
  volumeClaimTemplates:
  - metadata:
      name: redis-data
    spec:
      accessModes:
        - ReadWriteOnce
      storageClassName: "standard"
      resources:
        requests:
          storage: 100Mi
```
1.2.2 Deploy the new configuration
```sh
kubectl --namespace msa-demo apply -f redis-sts-pvc-t.yaml
```
1.2.3 And look at the `PV` and `PVC` that was created
```sh
kubectl --namespace msa-demo get pvc
```
Should yield:
```
NAME                 STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
redis-data-redis-0   Bound    pvc-3f5dccb7-6ae7-4879-8364-eb1e0798f3f0   100Mi      RWO            standard       14s
redis-data-redis-1   Bound    pvc-3ba1a22b-1080-4691-8945-b70846ceb037   100Mi      RWO            standard       10s
redis-data-redis-2   Bound    pvc-66ece253-6313-4e06-9cbb-7e00dc0c7fbd   100Mi      RWO            standard       5s``
```
```sh
kubectl get pv
```
Should yield:
```NAME                                       CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM                         STORAGECLASS   REASON   AGE
pvc-3ba1a22b-1080-4691-8945-b70846ceb037   100Mi      RWO            Delete           Bound    msa-demo/redis-data-redis-1   standard                2m57s
pvc-3f5dccb7-6ae7-4879-8364-eb1e0798f3f0   100Mi      RWO            Delete           Bound    msa-demo/redis-data-redis-0   standard                3m1s
pvc-66ece253-6313-4e06-9cbb-7e00dc0c7fbd   100Mi      RWO            Delete           Bound    msa-demo/redis-data-redis-2   standard                2m52s
```
# ConfigMap
### 2.1 Create an environment file
2.1.1 Create a file named `pinger-env` and write 2 variable defenition into it:
```
api-url=msa-api:8080
debug=true
```
2.1.2 Save the file and create a config-map out of it:
```sh
 kubectl create configmap pinger --from-env-file pinger-env --dry-run=client -oyaml >> pinger-cm.yaml
```
Should yield:
```yaml
apiVersion: v1
data:
  api-url: msa-api:8080
  debug: "true"
kind: ConfigMap
metadata:
  creationTimestamp: null
  name: pinger
```
2.1.3 Deploy the configMap to our namespace:
```sh
kubectl --namespace msa-demo apply -f pinger-cm.yaml
```
Verify CM was created:
```sh
kubectl --namespace msa-demo get cm
```
Should yield:
```
NAME     DATA   AGE
pinger   2      15m
```
2.1.4 Edit `msa-pinger.yaml` from previous session

In the `env` section in the container spec remove the first `value` entry (for `name: API_URL`) and put instead:
```yaml
      valueFrom:
        configMapKeyRef:
          name: pinger
          key: api-url
```
Replace the second `value` (for `name: debug`) with:
```yaml
      valueFrom:
        configMapKeyRef:
          name: pinger
          key: debug
```
2.1.5 Save the file and deploy it:
```sh
kubectl apply -f msa-pinger.yaml
```
Make sure pinger works properly.
