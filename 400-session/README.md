# HELM
## 0.1 get clean cluster
### 0.1.1 If you have a cluster from last session:
Delete it
```sh
minikube delete
```
### 0.1.2 Create new empty cluster
```sh
minikube start
```
### 0.1.2 Re-create the `msa-demo` namespace
```sh
kubectl create namespace msa-demo
```
## 1 Install redis chart
We will use the redis chart maintained by bitnami from the bitnami repository at
https://charts.bitnami.com/bitnami
### 1.1 Register the repository 
```sh
 helm repo add bitnami https://charts.bitnami.com/bitnami
```
### 1.2 Install redis with some specific values:
```sh
helm -n msa-demo install redis --set image.tag='5.0' --set cluster.enabled=false --set usePassword=false --version 12.7.4 bitnami/redis
```
In https://github.com/helm/charts/blob/master/stable/redis/values.yaml you can
find the default values of the redis chart. The default value.yaml is a good
source of knowledge regarding the ways one can modify the deployment details of
the application.
1.2.1 Verify redis is up and running
```sh
kubectl -n msa-demo get all
```
How can you verify the values you provided were actually used?
## 2 Create chart for the msa-api and install it
We will use the built-in scaffold to create a chart and modify it to deploy
msa-api
### 2.1 Create the chart
```sh
helm create msa-api
```
Verify the chart was created:
```sh
tree msa-api
```
Should yield:
```sh
msa-api
├── charts/
├── templates/
│   ├── tests/
│   │   └── test-connection.yaml
│   ├── deployment.yaml
│   ├── _helpers.tpl
│   ├── hpa.yaml
│   ├── ingress.yaml
│   ├── NOTES.txt
│   ├── serviceaccount.yaml
│   └── service.yaml
├── Chart.yaml
├── .helmignore
└── values.yaml
```
### 2.2 Modify/Add needed values
Open the `msa-api/values.yaml` file in your editor

2.2.1 Search for the `image:` section and replace the `repository:` from `nginx` to
`shelleg/msa-api` and the `tag:` from empty string to `config`. The `image:`
section should look like this:
```yaml
image:
  repository: shelleg/msa-api
  pullPolicy: IfNotPresent
  # Overrides the image tag whose default is the chart appVersion.
  tag: "config"
```
2.2.2 Add the following values anywhere in  the file. Make sure the indentation
starts from the 0 level.
```yaml
port: 8080

redisConf:
  host: redis-master
  port: 6379
```
Search for the `service:` section and modify the `port:` from `80` to `8080`
```yaml
service:
  type: ClusterIP
  port: 8080
```
The redis host is the name of the redis `service` object created by the redis
chart. We found it by running `kubectl -n msa-demo get services`
### 2.3 Modify the deployment template
Open the `msa-api/templates/deployment.yaml` file

2.3.1 Search for the `container:` section. At the same indentation as the `name:` add:
```yaml
          env:
            - name: REDIS_URL
              value: redis://{{ .Values.redisConf.host }}:{{ .Values.redisConf.port }}
```
2.3.2 Search for the `ports:` section and replace the `containerPort:` value to
`{{ .Values.port }}`. It should look like this:
```yaml
          ports:
            - name: http
              containerPort: {{ .Values.port }}
              protocol: TCP
```
### 2.4 Install the msa-api chart
```sh
helm -n msa-demo install msa-api ./msa-api
```
Verify the api is working:
```sh
kubectl -n msa-demo get all
```
Should yield something like:
```sh

NAME                           READY   STATUS    RESTARTS   AGE
pod/msa-api-67d855875b-fchm6   1/1     Running   0          12m
pod/redis-master-0             1/1     Running   0          111m

NAME                     TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)    AGE
service/msa-api          ClusterIP   10.100.30.91     <none>        80/TCP     12m
service/redis-headless   ClusterIP   None             <none>        6379/TCP   111m
service/redis-master     ClusterIP   10.101.190.207   <none>        6379/TCP   111m

NAME                      READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/msa-api   1/1     1            1           12m

NAME                                 DESIRED   CURRENT   READY   AGE
replicaset.apps/msa-api-67d855875b   1         1         1       12m

NAME                            READY   AGE
statefulset.apps/redis-master   1/1     111m
```
Now see if the api is accessible using port-forward
```sh
kubectl -n msa-demo port-forward service/msa-api 8080
```
Point your browser to `http://localhost:8080` to see if the api is up
## 3 Use dependency and values inheritance
### 3.1 First, remove redis installation
```sh
helm -n msa-demo delete redis
```
### 3.2 Add redis as dependency to msa-api
3.2.1 Open the `msa-api/charts.yaml` file in your editor

At the end of the file append:
```yaml
dependencies:
  - name: redis
    version: 12.7.4
    repository: https://charts.bitnami.com/bitnami
```
3.2.2 Build the dependency. (download the chart into the msa-api/charts folder)
```sh
helm dependency build msa-api
```
Verify build went well
```sh
tree msa-api
```
Should yield:
```sh
msa-api
├── charts/
│   └── redis-12.7.4.tgz
├── templates/
│   ├── tests/
│   │   └── test-connection.yaml
│   ├── deployment.yaml
│   ├── _helpers.tpl
│   ├── hpa.yaml
│   ├── ingress.yaml
│   ├── NOTES.txt
│   ├── serviceaccount.yaml
│   └── service.yaml
├── Chart.lock
├── Chart.yaml
├── .helmignore
└── values.yaml
```
See the `tgz` in the `charts` folder

3.2.3 Add the values to the values file

Open the msa-api values.yaml file in your editor. Add this section:
```yaml
redis:
  image:
    tag: '5.0'

  usePassword: false
  cluster:
    enabled: false
  fullnameOverride: redis
```
To the values we used in the direct installation we add another one:
`fullnameOverride`. We do it to disable the prefixing of all objects in the
`redis` release with the 'parent' chart name.

(You can see it by commenting this value and insatll/upgrade the chart)
### 3.3 deploy msa-api with redis as dependency
```sh
helm -n msa-demo upgrade msa-api ./msa-api
```
Verify that redis was installed:
```sh
kubectl -n msa-demo get all
```
Should yield something like:
```sh
NAME                           READY   STATUS    RESTARTS   AGE
pod/msa-api-67d855875b-cdhmg   1/1     Running   0          5m45s
pod/redis-master-0             1/1     Running   0          6m22s

NAME                     TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
service/msa-api          ClusterIP   10.100.30.91    <none>        80/TCP     3h36m
service/redis-headless   ClusterIP   None            <none>        6379/TCP   6m22s
service/redis-master     ClusterIP   10.103.63.211   <none>        6379/TCP   6m22s

NAME                      READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/msa-api   1/1     1            1           3h36m

NAME                                 DESIRED   CURRENT   READY   AGE
replicaset.apps/msa-api-67d855875b   1         1         1       3h36m

NAME                            READY   AGE
statefulset.apps/redis-master   1/1     6m22s
```
But the `redis` release does not exist as an helm release
```sh
helm -n list
```
Will only yield:
```sh
NAME    NAMESPACE       REVISION        UPDATED                                 STATUS          CHART           APP VERSION
msa-api msa-demo        2               2021-02-18 13:41:14.885956912 +0200 IST deployed        msa-api-0.1.0   1.16.0
```
Redis was embedded in the msa-api release.

Verify that everything works as expected. Use port forwarding.
## 4. Add msa-pinger
### 4.1 Create a chart for the pinger micro service
```sh
helm create msa-pinger
```
Verify the new chart was created
```sh
tree msa-pinger
```
Should yield:
```sh
msa-pinger
├── charts/
├── templates/
│   ├── tests/
│   │   └── test-connection.yaml
│   ├── deployment.yaml
│   ├── _helpers.tpl
│   ├── hpa.yaml
│   ├── ingress.yaml
│   ├── NOTES.txt
│   └── serviceaccount.yaml
├── Chart.yaml
├── .helmignore
└── values.yaml
```
### 4.2 modify deployment and add/modify the values
i4.2.1 Open the file `msa-pinger/values.yaml` in your editor. Change the `Image:`
section. Set the `repository:` value to: `shelleg/msa-pinger`

Add this section in indent 0:
```yaml
debug: 'true'
api:
  host: msa-api
  port: 8080
```
4.2.2 Open the `msa-pinger/templates/deployment.yaml` file and add to the
`containers` section in the `pinger` container this snippet:
```yaml
          env:
            - name: API_URL
              value: {{ .Values.api.host }}:{{ .Values.api.port }}
            - name: DEBUG
              value: {{ .Values.debug | quote }}
```
Make sure you have the correct indentation. The pipe and `quote` function ensure
the value will be wrapped with "" as an environment variable should.
4.2.3 Remove the service template since pinger does not need a service
```sh
rm msa-pinger/templates/service.yaml
```
### 4.3 Deploy the chart
```sh
helm -n msa-demo install msa-pinger ./msa-pinger
```
Use `kubectl get` and `kubectl port-forwarding` to verify pinger is running.
## 5 Umbrella
### 5.1 Delete all running charts
```sh
helm -n msa-demo delete msa-api
helm -n msa-demo delete msa-pinger
```
### 5.2 Create the Umbrella chart
```sh
helm create msa
```
5.2.1 remove the `templates folder` (This chart will not create objects of its
own)
```sh
rm -rf msa/templates
```
### 5.3 Move the existing charts to the `charts` folder of the umbrella
```sh
cp -r msa-api msa/charts/api
cp -r msa-pinger msa/charts/pinger
```
It should look like this:
```sh
tree msa
```
```sh
msa
├── charts/
│   ├── api/
│   │   ├── charts/
│   │   │   └── redis-12.7.4.tgz
│   │   ├── templates/
│   │   │   ├── tests/
│   │   │   │   └── test-connection.yaml
│   │   │   ├── deployment.yaml
│   │   │   ├── _helpers.tpl
│   │   │   ├── hpa.yaml
│   │   │   ├── ingress.yaml
│   │   │   ├── NOTES.txt
│   │   │   ├── serviceaccount.yaml
│   │   │   └── service.yaml
│   │   ├── Chart.lock
│   │   ├── Chart.yaml
│   │   ├── .helmignore
│   │   └── values.yaml
│   └── pinger/
│       ├── charts/
│       ├── templates/
│       │   ├── tests/
│       │   │   └── test-connection.yaml
│       │   ├── deployment.yaml
│       │   ├── _helpers.tpl
│       │   ├── hpa.yaml
│       │   ├── ingress.yaml
│       │   ├── NOTES.txt
│       │   └── serviceaccount.yaml
│       ├── Chart.yaml
│       ├── .helmignore
│       └── values.yaml
├── Chart.yaml
├── .helmignore
└── values.yaml
```
### 5.4 Deploy the umbrella
```sh
helm -n msa-demo install ./msa
```
Again, use `kubectl get` and `kubectl port-forward` to see what happend
