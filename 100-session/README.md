## Local Kubernetes environment

### Minikube
* Installation: https://minikube.sigs.k8s.io/docs/start/
* Start: 
  ```sh
  minikube start
  ```
* Run the gui dashboard:
  ```sh
  minikube dashboard
  ```
* Make sure you are in the right context:
  ```sh
  kubectl config get-context
  ```
* Run your first kubctl command:
  ```sh
  kubectl get nodes
  ```
* If you can split terminals try (you may need to install `watch`):
  ```sh
  watch -n0.1 kubectl get po
  ```
  Or
  ```sh
  kubectl get po --watch
  ```
* Run a pod:
  ```sh
  kubectl run nginx --image nginx
  ```
  
* See that it works:
  ```sh
  kubectl get pods --watch
  ```
* Cool kid's aliasing:
  ```sh
  alias k='kubectl'
  ```
### Some Homework
look for kubectl krew plugin manager at https://krew.sigs.k8s.io/
My recomendation, start with `ctx` and `ns` and add aliass:
```sh
alias kctx='kubectl-ctx'
alias kns='kubectl-ns'
```
